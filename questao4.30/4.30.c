#include <stdio.h>
#include <stdlib.h>

char token;

float exp(void);
float term(void);
float factor(void);

void error(void){
	fprintf(stderr, "Error\n");
	exit(1);
}

float potencia(float n, float x){
	float res = 1.0;
	if(x == 0){
		return 1;
	}
	if (x < 0)
		error();

	for (int i = x; i >= 1; i--){
		res *= n;
	}
	return res;
}

void match(char expectedToken){
	if(token == expectedToken)
		token = getchar();
	else{
		error();
	}
}

float exp(void){
	float temp = term();
	while ((token == '+') || (token=='-'))
		switch(token){
			case '+' :
				match('+');
				temp+=term();
				break;
			case '-':
				match('-');
				temp-=term();
				break;
		}
		return temp;
}

float exfactor(void){
	float temp = factor();
	switch(token){
		case '^':
			match('^');
			temp = potencia(temp, factor());
			break;
	}
	return temp;

}

float term(void){
	float temp = exfactor();
	while((token=='*') || (token == '/')|| (token == '%')){
		switch(token){
			case '*':
				match('*');
				temp *= exfactor();
				break;
			case '/':
				match('/');
				temp = temp/exfactor();
				break;
			case '%':
				match('%');
				temp = (float) ((int)temp % (int) exfactor());
				break;

		}
	}
	return temp;
}

float factor(void){
	
	float temp;
	if(token == '('){
		match('(');
		temp = exp();
		match(')');
	}
	else if(isdigit(token)){
		printf("%c", token);
		ungetc(token, stdin);
		scanf("%f", &temp);
		token = getchar();
	}
	else{
		error();
	}
	return temp;
}

int main(){
	float result;
	token = getchar();
	printf("%c\n", token);

	result = exp();

	
	if(token == '\n')
		printf("Result = %f\n", result);
	else{
		error();
	}
	return 0;
}
